package com.company;

public class Ex1 {
    public class A {
    }

    public class B extends A {
        C c;
        D d;
        private String param;
        public B(){
            c=new C();
        }
    }

    public class Z {
        public void g(B b){}
    }

    public class E {
        B b;
    }

    public class C {
    }

    public class D {
        public void f(){}
    }
}
