package com.company;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;

public class Ex2 {

    public static class GUI extends JFrame {
    JTextField text;
    JButton button;
    String numefisier="fisier.txt";
    public GUI(){
        setSize(400,400);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        text=new JTextField();
        text.setBounds(10,10,100,30);
        add(text);

        button=new JButton("Button");
        button.setBounds(10,40,100,30);
        add(button);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try{
                    FileWriter file=new FileWriter(numefisier);
                            file.write(text.getText());
                    file.close();
                }catch (IOException e){e.printStackTrace();}
            }
        });

        setVisible(true);


    }

        public static void main(String[] args) {
            new GUI();
        }
    }
}
